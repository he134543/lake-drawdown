# Lake Drawdown

A repository of hydrological modeling framework for lake drawdown management (HMF-Lake).

## Authors

**Xinchen He** (xinchenhe@umass.edu), Department of Civil and Environmental Engineering, University of Massachusetts, 130 Natural Resources Rd, Amherst, 01003, MA, USA

**Konstantinos Andreadis**, Department of Civil and Environmental Engineering, University of Massachusetts, 130 Natural Resources Rd, Amherst, 01003, MA, USA

**Allison H. Roy**, U.S. Geological Survey, Massachusetts Cooperative Fish and Wildlife Research Unit, Department of Environmental Conservation, University of Massachusetts, 160 Holdsworth Way, Amherst, 01003, MA, USA

**Abhishek Kumar**, Department of Environmental Conservation, University of Massachusetts, 160 Holdsworth Way, Amherst, 01003, MA, USA

**Caitlyn Butler**, Department of Civil and Environmental Engineering, University of Massachusetts, 130 Natural Resources Rd, Amherst, 01003, MA, USA

## Information

Repository Type: Python 3.10 scripts and jupyter notebooks

Year of Origin: 2021

Year of Version: 2023

Version: 1.0.0

Digital Object Identifier (DOI): pending

USGS Information Product Data System (IPDS) no.: pending

## Suggested Citation for Software

pending


## Basic Idea
We have developed a hydrological modeling framework which can simulate water level variations response to different weather conditions and operation rules.
The overall model structure are described below. 2 external GitHub repositories were used: [`RRMPG`](https://github.com/kratzert/RRMPG) and [`pyet`](https://github.com/pyet-org/pyet)

![Flow Chart](flow_chart.png)

In this study, we conducted 2 numerical experiments:
1. Historical drawdown Simulations: Operation rules were set as the real drawdown rules in the past. The simulated results were compared with actual historical water level monitoring results. The experiment was conducted in `notebooks/Experiment1_Run_View.ipynb`.
2. Assessing the ability of each winter drawdown lake to follow the [MA practical winter drawdown guidleine](https://www.mass.gov/doc/the-practical-guide-to-lake-management-in-massachusetts/download): Operation rules such as outflow and drawdown timings were set based on the guidelines and test whether the lake can achieve the guideline requirements: drawdown by Dec 1 and refill by Apr 1. The experiment results can be viewed in `notebooks/Experiment2_Run_View.ipynb`.

## Quick Start
### 1. System Requirements
- System: Linux (Tested in Manjaro and Ubuntu 22.03)
- Language: Python 3 (recommend using [Anaconda 3](https://www.anaconda.com/download))
- Package management tool: conda
- Storage: at least 250GB
- RAM: 12GB
- cpu threads: 15 (recommend)
### 2. Environment Set up
1. Directly create environment from `environment.yml`.

    NOT RECOMMEND: the environmental building process can take more than 15 minutes and there are some packages were from `pip` which may cause environment failure.

    ```bash
    (base) [xxx@xxx]: conda install nb_conda_kernels # Make sure you can swtich python environment in jupyter notebook/lab
    (base) [xxx@xxx]: conda env create -f environment.yml
    (base) [xxx@xxx]: conda activate wdmodel
    ```

2. Build environment by mannual install package denpendancies.

    ```bash
    (base) [xxx@xxx]: conda install nb_conda_kernels # Make sure you can swtich python environment in jupyter notebook/lab
    (base) [xxx@xxx]: conda create -n wdmodel python=3.10
    (base) [xxx@xxx]: conda activate wdmodel
    (wdmodel) [xxx@xxx]: conda install numpy pandas scipy matplotlib seaborn ipykernel pip 
    (wdmodel) [xxx@xxx]: pip install rasterio streamstats numba # Some packages are easier to install from pip
    ```

### 3. Run and View Experiment 1 and Experiment 2

After you configured the environment, go to jupyter or notebook and swtich environment to `wdmodel`. 

- Experiment 1: Re-creating historical water levels in winter drawdown lakes and comparing with in-situ observations. Users can follow the instructions of the notebook `notebooks/Experiment1_Run_and_View.ipynb` to run and view results of this experiment



- Experiment 2: Simulating water levels if the lake follow the Massachusetts winter drawdown guidleines. This experiment can be conducted by executing the script `run_exp2.py`, and the results can be interpreted and viewed in `notebooks/Experiment2_Results_View.ipynb`.
    
    For the first time user, you should change the `data_dir` in `run_exp2.py` and run command
    ```bash
    (base) [xxx@xxx] conda activate wdmodel
    (wdmodel) [xinchenh@XInchenmaj ~]$ python run_exp2.py
    ```

    **Important Note**: Be careful of the experiment 2 simulation, it will take a long time (~ 4 hours for 15 threads) to run and the output will take 200GB space (before compress). Please use `htop` to keep monitoring the process of the output generating.


## Explaination of the repository structure
### `models`
- `WDLake`: the hydrological modeling framework that we designed for lake water level drawdown management (HMF-Lake).
- Cloned repository from [pyet](https://github.com/pyet-org/pyet) and [RRMPG](https://github.com/kratzert/RRMPG). 

### `docs`
Saved the texts, figures in our manuscript

### `notebooks`
Jupyter notebooks to run and view HMF-Lake simulation results.

### `data`
Data and parameter files needed for running the model

### `tools`
Tool functions used in data analysis and visualization.


