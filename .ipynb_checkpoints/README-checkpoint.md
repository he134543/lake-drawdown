# Lake Drawdown

The repository of hydrological modeling framework for lake drawdown management (HMF-Lake).

## Basic Idea
- Model Structure
![Flow Chart](flow_chart.png)

- Experimental Designs
1. Historical drawdown Simulations -- Compare with the real historical winter drawdown levels
2. Assessing the ability of each winter drawdown lake to follow the [MA practical winter drawdown guidleine](https://www.mass.gov/doc/the-practical-guide-to-lake-management-in-massachusetts/download)

## Quick Start
### 1. Set Up
- System: Linux (Tested in Manjaro)

- Package management tool: conda

    Run the command below to create the conda environment of this project.

    ```bash
    conda env create -f environment.yml
    conda activate wdmodel
    ```
    
- Storage: at least 250GB

- RAM: 12GB

- Threads: 15 (recommend)


### 2. Numerical Experiment
We have 2 numerical experiment by using the HMF-Lake. 
- Experiment 1: Re-creating historical water levels in winter drawdown lakes and comparing with in-situ observations. Users can follow the instructions of the notebook `notebooks/Experiment1_Run_and_View.ipynb`.

- Experiment 2: Simulating water levels if the lake follow the Massachusetts winter drawdown guidleines. 
For the first time user, you should change the `data_dir` in run_exp2.py and run command
    ```bash
    (wdmodel) [xinchenh@XInchenmaj ~]$ python run_exp2.py
    ```
Users can view the results by following the instructions of the notebook `notebooks/Experiment2_Run_and_View.ipynb`.


**Important Note**: Be careful of the experiment 2 simulation, it will take a long time (~ 4 hours for 15 threads) to run and the output will take 200GB space (before compress). Make sure you selected a right file path to save the output.

## Explaination of the repository structure
### `models`
- `WDLake`: the hydrological modeling framework that we designed for lake water level drawdown management (HMF-Lake).
- Cloned repository from [pyet](https://github.com/pyet-org/pyet) and [RRMPG](https://github.com/kratzert/RRMPG). 

### `docs`
Saved the texts, figures in our manuscript

### `notebooks`
Jupyter notebooks to run and view HMF-Lake simulation results.

### `data`
Data and parameter files needed for running the model

### `tools`
Tool functions used in data analysis and visualization.



