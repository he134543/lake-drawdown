'''
This script is written for finding the latest refill date and the maximum drawdown level
'''

import numpy as np
import pandas as pd
from models.WDLake.WD_Lake import Lake



def find_refill_date(Lake, refill_days,  wd_timing = [1,2,3,4], operation_coeffs = [1,1,1], Q_lim = [], inflow_type = 'net_inflow', buffer = 0.05):
    """_This function is to find the best refill date of the lake_
       _Loop through a continous date range as the refill starting date_
       _untill the water level at April 1st is -0.05m_

    Args:
        Lake (_Lake object_): _Already loaded Lake object_
    """

    # Check years of the simulation time period
    # years = list(Lake.sim_date[:-1].year.unique())

    # create a list to save the water level at April 1st
    rwls_ap1 = []


    # Loop each refill date
    for i, t_refill in enumerate(refill_days):

        print("Setting {t_refill} as the refill date".format(t_refill = t_refill))
        
        # replace t3 with current refill date
        wd_timing[2] = t_refill

        # Routing
        Lake.Simulate(operation = True, wd_timing = wd_timing, operation_coeffs = operation_coeffs, Q_lim = Q_lim, inflow_type = inflow_type)
        
        # Extract the sim_rwls when using t[i] as refill date
        sim_rwl = Lake.sim_rwl.copy()

        # The relavive water level at every April 1st
        rwl_ap1 = sim_rwl.loc[sim_rwl.index.month == 4]
        rwl_ap1 = rwl_ap1.loc[rwl_ap1.index.day == 1]

        # save the rwls in april 1st
        rwls_ap1.append(rwl_ap1)

        # release RAM
        year_num = len(rwls_ap1[0])
        best_refill_dates = [0 for i in range(year_num)]
        years = [d.year for d in rwls_ap1[0].index] #get all years

        for i, y in enumerate(years): # Loop each year
            # This is year y
            for j, rwl_ap1 in enumerate(rwls_ap1): # Loop each rwl when selecting different refill date
                if rwl_ap1.iloc[i].values[0] >= -buffer: # 0.05 is the Buffer
                    # Sucessfully refilled, no
                    if j == len(rwls_ap1):# last date
                        best_refill_dates[i] = pd.to_datetime(str(y) + str(refill_days[j]), format="%Y%j")
                    continue
                else:
                    # First time not refilled, mark as the best refill date
                    best_refill_dates[i] = pd.to_datetime(str(y) + str(refill_days[j]), format="%Y%j")
                    break
    return best_refill_dates


def FRD(lname, bath_path, obs_rwl_path, obs_outflow_path, weather_type, weather_path, contributing_area, height, parea, wl_reduction, lat, 
   val_date, sim_date, t1,t2,t3s,t4,alpha,beta,gamma, inflow_type, model):
    """
    Find best refill dates
    """
    
    # import observations
    obs_rwl = pd.read_csv(obs_rwl_path, index_col=0, parse_dates=True) # m
    obs_outflow = pd.read_csv(obs_outflow_path, index_col=0, parse_dates=True).interpolate(method = 'time') * 86400 # m3/d

    # Initiate a Lake object
    lak = Lake(lname = lname, sim_date = sim_date, val_date = val_date)

    # Basic Properties
    #Lake.query_elevation()
    lak.contributing_area = contributing_area
    lak.height = height
    lak.lat = lat

    Qmin = 0.5 * lak.contributing_area/2.59e+6 * 0.0283168 * 86400# 0.5 cfsm TO m3/d
    Qmax = 4 * lak.contributing_area/2.59e+6 * 0.0283168 * 86400# 4 cfsm To m3/d


    lak.read_bathymetry(bath_path=bath_path, nSteps=100, unit = 'ft')
    lak.load_pvol_dvol(pvol = None, parea = parea, wl_reduction = wl_reduction)
    lak.load_observations(obs_rwl = obs_rwl, obs_outflow = obs_outflow, obs_area = [])
    lak.load_weather(weather_type=weather_type, csv_path=weather_path)
    lak.cal_net_inflow(set_lim = [0, 400000], smooth = True, window_length = 11, polyorder = 5)
    lak.connect_cgr4j(model=model, watershed_size_scale=False)
    best_refill_dates_lak = find_refill_date(lak, t3s, wd_timing = [t1,t2,0,t4], operation_coeffs = [alpha, beta, gamma], Q_lim = [Qmin, Qmax], inflow_type = inflow_type, buffer = 0.05)
    
    return best_refill_dates_lak