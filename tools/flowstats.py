import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def flow_duration(data, plot = False):
    """_Calculate flow duration statistics and plot_

    Args:
        data (_np.array_): _The flow time series or array_
    """
    
    if isinstance(data, pd.DataFrame):
        data = data.values.ravel()
    elif isinstance(data, pd.Series):
        data = data.values.ravel()
    else:
        data = data
        
    sort = np.sort(data)[::-1]
    exceedence = np.arange(1.,len(sort)+1) / len(sort)

    if plot == True:        
        plt.plot(exceedence*100, sort)
        plt.xlabel("Exceedence [%]")
        plt.ylabel("Flow rate")
        plt.show()
    
    df = pd.DataFrame(np.c_[exceedence, sort], columns=['Exceedence', 'Discharge'])
    df = df.set_index('Exceedence')
    
    return df