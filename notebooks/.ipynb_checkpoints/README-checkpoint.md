- `Experiment_1_Run_View.ipynb`: Experiment 1 in our paper. Use the actual historical rules to recreate historical water levels. This is a deterministic simulation.

- `Experiment_2_Run_View.ipynb`: Experiment 2 in our paper. Simulate water levels with different operation rules and evaluate the ability to achieve the state winter drawdown guidelines. This is a stochastic simulation.

- `Inflow_save_stochastic_simulation.ipynb`: Because in the stochastic simulation, the lake inflow simulation will take a long time. It is better to save the stochastic simulated inflow first for faster experiment 2 simulation.

- `Inflow_statistics_summarize.ipynb`: Summarizing the inflows simulated above.

- `Print_Lake_Information.ipynb`: print out the basic information of study lakes.

- `Daymet_Uncertainty.ipynb`: Compare the daymet data and in-situ observations from GHCN, estimate the error distribution type and parameters.

- `Compress_outputs.ipynb`: A jupyter notebook to compress the output files from the stochastic simulations.