import numpy as np
import pandas as pd
import rasterio
from scipy.optimize import curve_fit
from scipy import interpolate
import matplotlib.pyplot as plt


def read_bathymetry(bath_path, nSteps = 100, unit = 'ft', plot_fit_curve = False):
    """_Read the bathymetry file to get v_h, h_v, v_a relationship, and choose the highest depth as the emergency spillway height_

    Args:
        bath_path (_type_): _description_
        nSteps (int, optional): _Specify how many layers we would like to separate bathymetry files_. Defaults to 100.
        unit (str, optional): _Specify ft or meter used in the bathymetry file_. Defaults to 'meter'.

    Return:
        v_a, h_v, v_h, evol, ewl: relationships and emergency spillway vol and height

    """
    print('============Reading Bathymetry=============')

    lakeRst = rasterio.open(bath_path)
    
    # unit
    if unit == 'ft':
        lakeBottom = lakeRst.read(1) * 0.3048 # ft to m
    else: # unit == 'meter' 
        lakeBottom = lakeRst.read(1)

    # replace value for np.nan
    noDataValue = np.copy(lakeBottom[0,0])
    lakeBottom[lakeBottom==noDataValue]= np.nan
    # get raster minimum and maximum 
    minElev = np.nanmin(lakeBottom)
    maxElev = np.nanmax(lakeBottom)
    # lake bottom elevation intervals
    elevSteps = np.round(np.linspace(minElev,maxElev,nSteps),2)

    # calculate volumes for each elevation
    volArray = []
    areaArray = []

    # define of volume function
    def calculateVol(elevStep,elevDem,lakeRst):
        tempDem = elevDem[elevDem<=elevStep]
        tempVol = tempDem.sum()*lakeRst.res[0]*lakeRst.res[1]
        temp_area = len(tempDem) * lakeRst.res[0]*lakeRst.res[1]
        return tempVol, temp_area

    for elev in elevSteps:
        tempVol, temp_area = calculateVol(elev,lakeBottom,lakeRst)
        volArray.append(tempVol)
        areaArray.append(temp_area)

    # Fit with br1 * V ** br2
    def func(x, a, b):
        return a * x **b
    x = volArray
    y = areaArray
    br1, br2 = curve_fit(func,x,y)[0] # get the coefficients for vol_area
    v_a = (br1, br2) # get v-a relationship
    print('v_a loaded')

    # fit the curve of the vol and h. Use 5 degree polynomial regression.Need to pass through (0,0)   
    #f1 = np.poly1d(np.polyfit(elevSteps,volArray,3))
    #f2 = np.poly1d(np.polyfit(volArray,elevSteps,3))

    #f1 = interpolate.InterpolatedUnivariateSpline(elevSteps, volArray)
    #f2 = interpolate.InterpolatedUnivariateSpline(volArray, elevSteps)
    def func(x, a, b):
        return a * x **b
    x = volArray
    y = elevSteps
    a, b = curve_fit(func,x,y)[0] # get the coefficients for vol_area
    def f1(v):
        h = a * v ** b
        return h
    def f2(h):
        v = (h/a)**(1/b)
        return v
    
    h_v = f2
    print('h_v loaded')
    v_h = f1
    
    print('v_h loaded')
    
    # Load evol and ewl
    ewl = elevSteps[-1]
    evol = h_v(ewl)
        
    if plot_fit_curve == True:
        plt.figure()
        plt.title("Volume to H")
        plt.plot(volArray, f1(volArray))
        plt.scatter(volArray, elevSteps)
        plt.show()
    else:
        pass
    
    return v_a, h_v, v_h, evol, ewl

def plot_bath(lakename, bath_path, unit = 'ft', savedir = None):
    """_A function to plot the bathymetry_

    Args:
        lakename (_str_): _Name of the lake_
        bath_path (_str_): _Bathemetry file path of the lake_
        unit (_str_): 'ft' or 'm', indicate what's unit of the bathymetry
        save_dir (_str_): filepath of the directory you want to save the figure
    """
    lakeRst = rasterio.open(bath_path)
        # unit
    if unit == 'ft':
        lakeBottom = lakeRst.read(1) * 0.3048 # ft to m
    else: # unit == 'meter' 
        lakeBottom = lakeRst.read(1)

    fig, ax1 = plt.subplots(1,1,figsize=(8,6))
    ax1.set_title('Bathymetry')
    botElev = ax1.imshow(lakeBottom)
    fig.colorbar(botElev, orientation='horizontal', label='WaterDepth m')
    plt.suptitle(lakename)
    if savedir == None:
        plt.show()
    else:
        plt.savefig(savedir + lakename + '_bath.png')