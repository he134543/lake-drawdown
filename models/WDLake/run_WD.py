from multiprocessing import set_start_method
from telnetlib import STATUS
import numpy as np
import pandas as pd
from numba import jit
from models.WDLake import dam_operation
from models.pyet.pyet.radiation import oudin

# For running a WD in one year
@jit(nopython=True)
def run_WD(Initials, sim_days, 
           wd_timings, operation_coeffs, Qlim, evol, pvol, dvol, 
           I, P, E, v_a):
    '''
    Note, this function multiply the Volume difference rather than divide.
    '''
    # Unpack parameters
    # Drawdown Start and Refill Start
    # print(wd_timings)
    t1,t2,t3,t4 = wd_timings
    alpha, beta, gamma, theta = operation_coeffs
    # overbank --> fast release, no coefficient
    # alpha  --> drawdown coef
    # beta   --> stable coef
    # gamma  --> refill coef
    # theta  --> non-drawdown season, summer
    Qmin_recession, Qmin_refill, Qmin_drawdown, Qmin_summer, Qmax_recession, Qmax_refill, Qmax_drawdown, Qmax_summer = Qlim
    # alpha -- Recession, beta -- Refill, gamma -- Refill
    
    # Enter the empty storage and area array, from 0 to t
    # for water balance routing
    # Set intial values
    V = np.zeros(len(sim_days))
    V[0] = Initials[0]
    A = np.zeros(len(sim_days))
    A[0] = Initials[1]
    
    # Create an empty Q, from 1 to t
    Q = np.zeros(len(sim_days[:-1]))
    
    def selectQ(Q, Qmin, Qmax):
        if Q > Qmax:
            return Qmax
        if Q < Qmin:
            return Qmin
        else:
            return Q
    
    if sim_days[0] < 100:
        raise ValueError("Please set the sim date starting after May 1st")
    
    # Loop every day
    for i, t in enumerate(sim_days[:-1]):
        # Before the routing, make sure what's the phase of today
        # Emulate Q first
        #if V[i] >= evol: # No matter what today is, if the volume reach the emergency volume
            #V[i] = evol
        #    status = "overbank"
        #    Q[i] = I[i] + V[i] - evol
            

        if t < t1: # before drawdown, target is pvol, summer
            status = "summer"
            Q[i] = selectQ(I[i] + (V[i] - pvol)/theta, Qmin_summer, Qmax_summer)

        elif t1 <= t < t3: # Before the refill start date, could be recession or drawdown
            # Once the water level reach the dvol, the drawdown would be counted as completed
            if status == "drawdown":# Status can be recession and drawdown
                if V[i] >= dvol : # If haven't reach dvol, then classify as recession
                    status = "recession"
                    Q[i] = selectQ(I[i] + (V[i] - dvol)/alpha, Qmin_recession, Qmax_recession)
                else:
                    status = "drawdown" # once it reaches dvol, turn into drawdown
                    Q[i] = selectQ(I[i] + (V[i] - dvol)/beta, Qmin_drawdown, Qmax_drawdown)
            else: # Haven't reac dvol yet
                status == "recession"
                Q[i] = selectQ(I[i] + (V[i] - dvol)/alpha, Qmin_recession, Qmax_recession)

        elif t >= t3: # Start Refill, target is pvol again
            # it can be refill or summer
            if status == "summer":
                Q[i] = selectQ(I[i] + (V[i] - pvol)/theta, Qmin_summer, Qmax_summer)
            else:
                if V[i] < pvol:
                    status = "refill"
                    Q[i] = selectQ(I[i] + (V[i] - pvol)/gamma, Qmin_refill, Qmax_refill)
                else: # Once reach normal volume. To refill phase
                    status = "summer"
                    Q[i] = selectQ(I[i] + (V[i] - pvol)/theta, Qmin_summer, Qmax_summer)

        else:
            raise ValueError("Routing Failure")
        
        # Then routing
        V[i+1] = V[i] + I[i] - Q[i] + (P[i]-E[i]) * A[i] # dV/dt = I(t) - Q(t) - Qs[i] + (precip - pet) * A(t)
        if V[i+1] > evol:
            V[i+1] = evol
        
        # Area, A = col1 * V ** col2
        A[i+1] = v_a[0] * V[i+1] ** v_a[1]
        
    return (V, A, Q)


@jit(nopython=True)
def run_WD_stochastic(Initials, 
                      sim_days, 
                      wd_timings, 
                      operation_coeffs_range, 
                      Qlim, evol, pvol, dvol,
                      I, P, E,
                      v_a,
                      iteration_times):
    """_This function is to run the winter drawdown model stochastically. Support to include weather uncertainty and release rate uncertainty_

    Args:
        Initials (_type_): _description_
        sim_days (_type_): _description_
        wd_timings (_type_): _description_
        operation_coeffs (_list_): _Has to be a tuple of lists, folllow the sequence of alpha, beta, gamma, theta. E.g., [alpha]_
        Qlim (_type_): _description_
        evol (_type_): _description_
        pvol (_type_): _description_
        dvol (_type_): _description_
        weather_uncertainty (_type_): _description_
        I (_type_): _description_
        P (_type_): _description_
        E (_type_): _description_
        v_a (_type_): _description_
    """

    # Load the range of parameters
    alpha_range, beta_range, gamma_range, theta_range = operation_coeffs_range
    t1,t2,t3,t4 = wd_timings
    Qmin_recession, Qmin_refill, Qmin_drawdown, Qmin_summer, Qmax_recession, Qmax_refill, Qmax_drawdown, Qmax_summer = Qlim
    # alpha -- Recession, beta -- Refill, gamma -- Refill
    # Enter the empty storage and area array, from 0 to t
    # for water balance routing
    # Set intial values
    V = np.zeros((iteration_times, len(sim_days)))
    V[:,0] = Initials[0]
    A = np.zeros((iteration_times, len(sim_days)))
    A[:,0] = Initials[1]
    
    # Create an empty Q, from 1 to t
    Q = np.zeros((iteration_times, len(sim_days[:-1])))
    
    def selectQ(Q, Qmin, Qmax):
        if Q > Qmax:
            return Qmax
        if Q < Qmin:
            return Qmin
        else:
            return Q
    
    if sim_days[0] < 100:
        raise ValueError("Please set the sim date starting after May 1st")
    

    # Generate alphas with a length of len(sim_days) -1 
    # "Size" is not supported in Numba
    # In numba, the parameter names were changed from "high" or "low" to "a", "b"
    # alphas = np.random.uniform(alpha_range[0], alpha_range[-1], size= len(sim_days[:-1]))
    # betas = np.random.uniform(beta_range[0], beta_range[-1], size = len(sim_days[:-1]))
    # gammas = np.random.uniform(gamma_range[0], gamma_range[-1], size = len(sim_days[-1]))
    # thetas = np.random.uniform(theta_range[0], theta_range[-1], size = len(sim_days[-1]))

    # Loop every day
    for iter in range(iteration_times):

        status = "recession"
        for i, t in enumerate(sim_days[:-1]):
            # Before the routing, make sure what's the phase of today
            # Emulate Q first
            #if V[i] >= evol: # No matter what today is, if the volume reach the emergency volume
                #V[i] = evol
            #    status = "overbank"
            #    Q[i] = I[i] + V[i] - evol

            alpha = np.random.uniform(alpha_range[0], alpha_range[-1])
            beta =  np.random.uniform(beta_range[0], alpha_range[-1])
            gamma = np.random.uniform(gamma_range[0], alpha_range[-1])
            theta = np.random.uniform(theta_range[0], alpha_range[-1])
            
            

            if t < t1: # before drawdown, target is pvol, summer
                status = "summer"
#                Q[iter,i] = selectQ(I[iter,i] + (V[iter,i] - pvol)/theta, Qmin_summer, Qmax_summer)
                
                # when t is approaching the Dec 1, the maximum alpha would minus 1
                max_alpha = max(1, t2-t)
                alpha = np.random.uniform(0, max_alpha)
                Q[iter,i] = selectQ(I[iter,i] + (V[iter,i] - dvol)/alpha, Qmin_recession, Qmax_recession)

            elif t1 <= t < t3: # Before the refill start date, could be recession or drawdown
#                if status == "drawdown":# Status can be recession and drawdown
#                    if V[iter,i] >= dvol : # If haven't reach dvol, then classify as recession
#                        status = "recession"
#                        Q[iter,i] = selectQ(I[iter,i] + (V[iter,i] - dvol)/alpha, Qmin_recession, Qmax_recession)
#                    else:
#                        status = "drawdown" # once it reaches dvol, turn into drawdown
#                        Q[iter,i] = selectQ(I[iter,i] + (V[iter,i] - dvol)/beta, Qmin_drawdown, Qmax_drawdown)
#                else: # Haven't reac dvol yet
#                    status == "recession"
#                    Q[iter,i] = selectQ(I[iter,i] + (V[iter,i] - dvol)/alpha, Qmin_recession, Qmax_recession)

                # when t is approaching the Dec 1, the maximum alpha would minus 1
                status = "drawdown"
                max_alpha = max(1, t2-t)
                alpha = np.random.uniform(0, max_alpha)
                Q[iter,i] = selectQ(I[iter,i] + (V[iter,i] - dvol)/alpha, Qmin_recession, Qmax_recession)

            elif t >= t3: # Start Refill, target is pvol again
                # it can be refill or summer
                # if status == "summer":
                #     Q[iter,i] = selectQ(I[iter,i] + (V[iter,i] - pvol)/theta, Qmin_summer, Qmax_summer)
                # else:
                #     if V[iter,i] < pvol:
                #         status = "refill"
                #         Q[iter,i] = selectQ(I[iter,i] + (V[iter,i] - pvol)/gamma, Qmin_refill, Qmax_refill)
                #     else: # Once reach normal volume. To refill phase
                #         status = "summer"
                #         Q[iter,i] = selectQ(I[iter,i] + (V[iter,i] - pvol)/theta, Qmin_summer, Qmax_summer)

                if status == "summer":
                    Q[iter,i] = selectQ(I[iter,i] + (V[iter,i] - pvol)/theta, Qmin_summer, Qmax_summer)
                else:
                    if V[iter,i] < pvol:
                        status = "refill"
                        Q[iter,i] = selectQ(I[iter,i] + (V[iter,i] - pvol)/gamma, Qmin_refill, Qmax_refill)
                    else: # Once reach normal volume. To refill phase
                        status = "summer"
                        Q[iter,i] = selectQ(I[iter,i] + (V[iter,i] - pvol)/theta, Qmin_summer, Qmax_summer)

            else:
                raise ValueError("Routing Failure")
            
            # Then routing
            V[iter,i+1] = V[iter,i] + I[iter,i] - Q[iter,i] + (P[i]-E[i]) * A[iter,i] # dV/dt = I(t) - Q(t) - Qs[i] + (precip - pet) * A(t)
            if V[iter,i+1] > evol:
                V[iter,i+1] = evol
            
            # Area, A = col1 * V ** col2
            A[iter,i+1] = v_a[0] * V[iter,i+1] ** v_a[1]
        
    return (V, A, Q)
    

# @jit(nopython = True)
def stochastic_weather(locs, scales, iteration_times, pr, tmax, tmin):
    corrected_pr = np.zeros((iteration_times, len(pr)))
    corrected_tmin = np.zeros((iteration_times, len(pr)))
    corrected_tmax = np.zeros((iteration_times, len(pr)))
    corrected_tmean = np.zeros((iteration_times, len(pr)))
    
    epsilon_pr = np.random.laplace(loc = locs[0], scale = scales[0], size = (iteration_times, len(pr))) # mm, length is the size number
    epsilon_tmax = np.random.laplace(loc = locs[1], scale = scales[1], size = (iteration_times, len(pr)))
    epsilon_tmin = np.random.laplace(loc = locs[2], scale = scales[2], size = (iteration_times, len(pr)))
    
    corrected_pr = epsilon_pr + pr # Broadcast to (iter, len(pr))
    corrected_pr[corrected_pr < 0] = 0 # need to be all positive
    corrected_tmax = epsilon_tmax + tmax
    corrected_tmin = epsilon_tmin + tmin
    corrected_tmean = (corrected_tmin + corrected_tmax)/2
    
    return corrected_pr, corrected_tmax, corrected_tmin, corrected_tmean


if __name__ == "__main__":
    sim_date = pd.date_range('2016-07-01', '2020-07-01')